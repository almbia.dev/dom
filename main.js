// window réagit au DOM et le message coucou s'affiche dans la console
window.addEventListener('DOMContentLoaded', function(event) {
    console.log('coucou');


    let tries;
    let randomNumber;

// cette fonction nous permet d'avoir un nombre aléatoire à   trouver
    function startGame() {
        tries = 1;
        randomNumber = Math.floor(Math.random() * 100);
        console.log('Le nombre à deviner est :' + randomNumber);
    }

    startGame();

    function submitForm(event) {
        event.preventDefault();
        const form = document.getElementById('guess_form');
        const value = parseInt(form.guess.value);
// soumission du formulaire ainsi que the sumbmitted value is
// s'afficheront une fois que la personne aura cliqué sur submit        
        console.log('soumission du formulaire');
        console.log('The submitted value is: ' + value);

// cette fonction nous permet d'afficher 
//si le nombre qu'on a donné est en dessous ou au dessus de celui qu'il faut trouver
        if (value > randomNumber) {
            alert('C\'est plus!!');
        } else if (value < randomNumber) {
            alert('C\'est moins!!');
        } else {
            alert('Bravo ! Tu as trouvé le bon nombre en ' + tries + ' essais.');
            startGame();
        }
// tries nous indique combien  de fois la personne a entrer un nombre avant de trouver le bon
        tries++;
    }
//  cette partie de code réagit à guess_form et permettra d'enregistrer les données une fois que la personne 
// cliquera sur submit
    const form = document.getElementById('guess_form');
    form.addEventListener('submit', submitForm);
});

